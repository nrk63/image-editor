package ru.miet.edu.imageeditor.model.operational;

public class ImageProcessing {
    public static double truncate(double channel) {
        if (channel < 0.0)
            return 0.0;
        return Math.min(channel, 1.0);
    }
}
