package ru.miet.edu.imageeditor.model;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import ru.miet.edu.imageeditor.model.operational.ImageOperation;

public class ProgramState {
    public ProgramState(WritableImage currentImage, WritableImage displayImage) {
        this.currentImageProperty = new SimpleObjectProperty<>(currentImage);
        this.displayImageProperty = new SimpleObjectProperty<>(displayImage);
    }

    public void applyToDisplayImage(ImageOperation imageOperation) {
        WritableImage displayImage = displayImageProperty.getValue();
        WritableImage resultImage = imageOperation.apply(displayImage);
        setDisplayImage(resultImage);
    }

    public void applyToCurrentImage(ImageOperation imageOperation) {
        applyToDisplayImage(imageOperation);
        applyDisplayed();
    }

    public void applyDisplayed() {
        //TODO: save in history
        Image displayImage = displayImageProperty.getValue();
        WritableImage newCurrentImage = new WritableImage(displayImage.getPixelReader(),
                                                          (int)displayImage.getWidth(),
                                                          (int)displayImage.getHeight());
        currentImageProperty.setValue(newCurrentImage);
    }

    public void setDisplayImage(Image image) {
        WritableImage newDisplayImage = new WritableImage(image.getPixelReader(),
                                                          (int)image.getWidth(),
                                                          (int)image.getHeight());
        setDisplayImage(newDisplayImage);
    }

    public void setCurrentImage(Image image) {
        setDisplayImage(image);
        applyDisplayed();
    }

    public double getWidth() {
        return currentImageProperty.getValue().getWidth();
    }

    public double getHeight() {
        return currentImageProperty.getValue().getHeight();
    }

    public Image getDisplayImage() {
        return displayImageProperty.getValue();
    }

    public Image getCurrentImage() {
        return currentImageProperty.getValue();
    }

    public ReadOnlyObjectProperty<WritableImage> getDisplayImageProperty() {
        return displayImageProperty;
    }

    public ReadOnlyObjectProperty<WritableImage> getCurrentImageProperty() {
        return currentImageProperty;
    }

    private void setDisplayImage(WritableImage writableImage) {
        displayImageProperty.setValue(writableImage);
    }

    private void setCurrentImage(WritableImage writableImage) {
        setDisplayImage(writableImage);
        applyDisplayed();
    }

    private final SimpleObjectProperty<WritableImage> currentImageProperty;
    private final SimpleObjectProperty<WritableImage> displayImageProperty;
}
