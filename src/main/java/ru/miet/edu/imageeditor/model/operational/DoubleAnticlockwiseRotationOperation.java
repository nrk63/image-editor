package ru.miet.edu.imageeditor.model.operational;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class DoubleAnticlockwiseRotationOperation implements ImageOperation {
    @Override
    public WritableImage apply(Image image) {
        int imageWidth = (int)image.getWidth();
        int imageHeight = (int)image.getHeight();
        PixelReader reader = image.getPixelReader();
        WritableImage resultImage = new WritableImage(reader, imageWidth, imageHeight);
        PixelWriter writer = resultImage.getPixelWriter();
        for (int y = 0; y < imageHeight; y += 1) {
            for (int x = 0; x < imageWidth; x += 1) {
                Color color = reader.getColor(x, y);
                writer.setColor(x, imageHeight - 1 - y, color);
            }
        }
        return resultImage;
    }
}
