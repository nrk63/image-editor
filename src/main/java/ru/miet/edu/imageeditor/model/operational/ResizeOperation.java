package ru.miet.edu.imageeditor.model.operational;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

public class ResizeOperation implements ImageOperation {
    public ResizeOperation(double width, double height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public WritableImage apply(Image image) {
        BufferedImage awtImage = SwingFXUtils.fromFXImage(image, null);
        BufferedImage scaledAwtImage = new BufferedImage((int)width, (int)height, awtImage.getType());

        Graphics2D g2d = scaledAwtImage.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                             RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.drawImage(awtImage, 0, 0, scaledAwtImage.getWidth(), scaledAwtImage.getHeight(),
                                0, 0, awtImage.getWidth(), awtImage.getHeight(), null);
        g2d.dispose();
        return SwingFXUtils.toFXImage(scaledAwtImage, null);
    }

    private final double width;
    private final double height;
}
