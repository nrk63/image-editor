package ru.miet.edu.imageeditor.model.operational;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class ClockwiseRotationOperation implements ImageOperation {
    @Override
    public WritableImage apply(Image image) {
        int imageWidth = (int)image.getWidth();
        int imageHeight = (int)image.getHeight();
        WritableImage resultImage = new WritableImage(imageHeight, imageWidth);
        PixelReader reader = image.getPixelReader();
        PixelWriter writer = resultImage.getPixelWriter();
        for (int y = 0; y < imageWidth; y += 1) {
            for (int x = 0; x < imageHeight; x += 1) {
                Color color = reader.getColor(y, imageHeight - 1 - x);
                writer.setColor(x, y, color);
            }
        }
        return resultImage;
    }
}
