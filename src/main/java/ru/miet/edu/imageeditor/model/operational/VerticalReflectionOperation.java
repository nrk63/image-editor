package ru.miet.edu.imageeditor.model.operational;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class VerticalReflectionOperation implements ImageOperation {
    @Override
    public WritableImage apply(Image image) {
        PixelReader reader = image.getPixelReader();
        WritableImage resultImage = new WritableImage(reader, (int)image.getWidth(),
                                                      (int)image.getHeight());
        PixelWriter writer = resultImage.getPixelWriter();
        int imageHeight = (int)image.getHeight();
        for (int y = 0; y < imageHeight/2; y += 1) {
            for (int x = 0; x < (int)image.getWidth(); x += 1) {
                Color upperColor = reader.getColor(x, y);
                Color bottomColor = reader.getColor(x, imageHeight - 1 - y);
                writer.setColor(x, y, bottomColor);
                writer.setColor(x, imageHeight - 1 - y, upperColor);
            }
        }
        return resultImage;
    }
}
