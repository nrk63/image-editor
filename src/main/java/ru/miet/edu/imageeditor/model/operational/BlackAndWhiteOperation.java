package ru.miet.edu.imageeditor.model.operational;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class BlackAndWhiteOperation implements ImageOperation {
    @Override
    public WritableImage apply(Image image) {
        PixelReader reader = image.getPixelReader();
        WritableImage resultImage = new WritableImage(reader, (int)image.getWidth(),
                                                      (int)image.getHeight());
        PixelWriter writer = resultImage.getPixelWriter();
        for (int y = 0; y < (int)image.getHeight(); y += 1) {
            for (int x = 0; x < (int)image.getWidth(); x += 1) {
                Color color = reader.getColor(x, y);
                double max = Math.max(color.getRed(), Math.max(color.getGreen(), color.getBlue()));
                writer.setColor(x, y, Color.color(max, max, max, color.getOpacity()));
            }
        }
        return resultImage;
    }
}
