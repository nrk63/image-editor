package ru.miet.edu.imageeditor.model.operational;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class HorizontalReflectionOperation implements ImageOperation {
    @Override
    public WritableImage apply(Image image) {
        PixelReader reader = image.getPixelReader();
        WritableImage resultImage = new WritableImage(reader, (int)image.getWidth(),
                                                      (int)image.getHeight());
        PixelWriter writer = resultImage.getPixelWriter();
        int imageWidth = (int)image.getWidth();
        for (int y = 0; y < (int)image.getHeight(); y += 1) {
            for (int x = 0; x < imageWidth/2; x += 1) {
                Color leftColor = reader.getColor(x, y);
                Color rightColor = reader.getColor(imageWidth - 1 - x, y);
                writer.setColor(x, y, rightColor);
                writer.setColor(imageWidth - 1 - x, y, leftColor);
            }
        }
        return resultImage;
    }
}
