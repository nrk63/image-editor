package ru.miet.edu.imageeditor.model.operational;

import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

public interface ImageOperation {
    WritableImage apply(Image image);
}
