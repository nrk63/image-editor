package ru.miet.edu.imageeditor.model.operational;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class SepiaOperation implements ImageOperation {
    @Override
    public WritableImage apply(Image image) {
        PixelReader reader = image.getPixelReader();
        WritableImage resultImage = new WritableImage(reader, (int)image.getWidth(),
                                                      (int)image.getHeight());
        PixelWriter writer = resultImage.getPixelWriter();
        for (int y = 0; y < (int)image.getHeight(); y += 1) {
            for (int x = 0; x < (int)image.getWidth(); x += 1) {
                Color color = reader.getColor(x, y);
                double red = color.getRed();
                double green = color.getGreen();
                double blue = color.getBlue();
                double newRed = ImageProcessing.truncate(0.393*red + 0.769*green + 0.189*blue);
                double newGreen = ImageProcessing.truncate(0.349*red + 0.686*green + 0.168*blue);
                double newBlue = ImageProcessing.truncate(0.272*red + 0.534*green + 0.131*blue);
                writer.setColor(x, y, Color.color(newRed, newGreen, newBlue, color.getOpacity()));
            }
        }
        return resultImage;
    }
}
