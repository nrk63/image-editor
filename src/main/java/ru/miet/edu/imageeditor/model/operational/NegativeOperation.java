package ru.miet.edu.imageeditor.model.operational;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class NegativeOperation implements ImageOperation {
    @Override
    public WritableImage apply(Image image) {
        PixelReader reader = image.getPixelReader();
        WritableImage resultImage = new WritableImage(reader, (int)image.getWidth(),
                                                      (int)image.getHeight());
        PixelWriter writer = resultImage.getPixelWriter();
        for (int y = 0; y < (int)image.getHeight(); y += 1) {
            for (int x = 0; x < (int)image.getWidth(); x += 1) {
                Color color = reader.getColor(x, y);
                Color newColor = Color.color(1.0 - color.getRed(),
                                             1.0 - color.getGreen(),
                                             1.0 - color.getBlue(),
                                             color.getOpacity());
                writer.setColor(x, y, newColor);
            }
        }
        return resultImage;
    }
}
