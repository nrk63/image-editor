package ru.miet.edu.imageeditor.view.events.operational;

import ru.miet.edu.imageeditor.model.operational.DoubleClockwiseRotationOperation;

public class DoubleClockwiseRotationOperationEvent extends ImageOperationEvent {
    public DoubleClockwiseRotationOperationEvent() {
        super(ImageOperationEvent.CURRENT, new DoubleClockwiseRotationOperation());
    }
}
