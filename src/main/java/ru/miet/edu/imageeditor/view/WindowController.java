package ru.miet.edu.imageeditor.view;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.*;
import ru.miet.edu.imageeditor.view.events.FileEvent;
import ru.miet.edu.imageeditor.view.events.operational.ImageOperationEvent;
import ru.miet.edu.imageeditor.model.ProgramState;
import ru.miet.edu.imageeditor.view.events.LanguageChangeEvent;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class WindowController implements Initializable {
    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {
        menuBarController.addMenuBarEventHandler(FileEvent.OPEN, event -> {
            Image image = new Image(event.getFile().getAbsolutePath());
            programState.setCurrentImage(image);
        });

        menuBarController.addMenuBarEventHandler(FileEvent.SAVE, event -> {
            File file = event.getFile();
            try {
                Image currentImage = programState.getCurrentImage();
                BufferedImage awtImage = SwingFXUtils.fromFXImage(currentImage, null);
                //FIXME: why png only?
                ImageIO.write(awtImage, "png", file);
                //TODO: warning dialog if ImageIO.write returns false
            } catch (IOException e) {
                e.printStackTrace();
                //TODO: error dialog
            }
        });

        menuBarController.addMenuBarEventHandler(LanguageChangeEvent.CHANGED, event -> {
            shutdown();
            LoadSceneParams loadSceneParams = (new LoadSceneParams.Builder())
                                                  .programState(programState)
                                                  .locale(event.getLocale())
                                                  .build();
            loadSceneMethod.accept(loadSceneParams);
        });

        menuBarController.addMenuBarEventHandler(ImageOperationEvent.DISPLAYED_ONLY, event -> {
            imageProcessingExecutor.execute(() ->
                    programState.applyToDisplayImage(event.getImageOperation()));
            //TODO: apply prompt
        });

        menuBarController.addMenuBarEventHandler(ImageOperationEvent.CURRENT, event -> {
            imageProcessingExecutor.execute(() ->
                    programState.applyToCurrentImage(event.getImageOperation()));
        });
    }

    public void setLoadSceneMethod(Consumer<LoadSceneParams> loadSceneMethod) {
        this.loadSceneMethod = loadSceneMethod;
    }

    public void shutdown() {
        imageProcessingExecutor.shutdown();
        imagePreview.imageProperty().unbind();
    }

    public void setProgramState(ProgramState programState) {
        this.programState = programState;
        imagePreview.setFitWidth(programState.getWidth());
        imagePreview.setFitHeight(programState.getHeight());
        imagePreview.imageProperty().bind(programState.getDisplayImageProperty());
    }

    private ProgramState programState;
    private final ExecutorService imageProcessingExecutor = Executors.newSingleThreadExecutor();

    private Consumer<LoadSceneParams> loadSceneMethod;

    @FXML
    private MenuBarController menuBarController;
    @FXML
    private ImageView imagePreview;
}
