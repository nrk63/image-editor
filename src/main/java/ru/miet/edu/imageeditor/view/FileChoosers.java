package ru.miet.edu.imageeditor.view;

import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.ResourceBundle;

public class FileChoosers {
    static File showOpenImageChooser(ResourceBundle resourceBundle) {
        Stage fileChooserStage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(resourceBundle.getString("fileChooser.openImage.title"));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(
                resourceBundle.getString("fileChooser.openImage.extensionFilter.description"),
                "*.png", "*.jpg", "*.gif", "*.bmp"));
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));

        File file = fileChooser.showOpenDialog(fileChooserStage);
        fileChooserStage.show();
        fileChooserStage.close();
        return file;
    }

    static File showSaveImageChooser(ResourceBundle resourceBundle) {
        Stage fileChooserStage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(resourceBundle.getString("fileChooser.saveImage.title"));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(
                resourceBundle.getString("fileChooser.saveImage.extensionFilter.description"),
                "*.png", "*.jpg", "*.gif", "*.bmp"));
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));

        File file = fileChooser.showSaveDialog(fileChooserStage);
        fileChooserStage.show();
        fileChooserStage.close();
        return file;
    }
}
