package ru.miet.edu.imageeditor.view.events.operational;

import ru.miet.edu.imageeditor.model.operational.VerticalReflectionOperation;

public class VerticalReflectionOperationEvent extends ImageOperationEvent {
    public VerticalReflectionOperationEvent() {
        super(ImageOperationEvent.CURRENT, new VerticalReflectionOperation());
    }
}
