package ru.miet.edu.imageeditor.view;

import javafx.scene.image.WritableImage;
import ru.miet.edu.imageeditor.model.ProgramState;

import java.util.Locale;
import java.util.Objects;

public class LoadSceneParams {
    public static class Builder {
        public Builder() {
            WritableImage currentImage = new WritableImage(DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT);
            WritableImage displayImage = new WritableImage(DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT);
            programState = new ProgramState(currentImage, displayImage);
        }

        public LoadSceneParams build() {
            return new LoadSceneParams(this);
        }

        public Builder programState(ProgramState programState) {
            Objects.requireNonNull(programState);
            this.programState = programState;
            return this;
        }

        public Builder locale(Locale locale) {
            Objects.requireNonNull(locale);
            this.locale = locale;
            return this;
        }

        private ProgramState programState;
        private Locale locale = DEFAULT_LOCALE;
    }

    public ProgramState getProgramState() {
        return programState;
    }

    public Locale getLocale() {
        return locale;
    }

    private LoadSceneParams(Builder builder) {
        this.programState = builder.programState;
        this.locale = builder.locale;
    }

    private final ProgramState programState;
    private final Locale locale;

    private static final int DEFAULT_IMAGE_WIDTH = 600;
    private static final int DEFAULT_IMAGE_HEIGHT = 600;
    private static final Locale DEFAULT_LOCALE = new Locale("ru", "RU");
}
