package ru.miet.edu.imageeditor.view.events.operational;

import ru.miet.edu.imageeditor.model.operational.ResizeOperation;

public class ResizeOperationEvent extends ImageOperationEvent {
    public ResizeOperationEvent(double width, double height) {
        super(CURRENT, new ResizeOperation(width, height));
    }
}
