package ru.miet.edu.imageeditor.view.events;

import javafx.event.Event;
import javafx.event.EventType;

import java.io.File;

public class FileEvent extends Event {
    public static final EventType<FileEvent> OPEN = new EventType<>(ANY, "OPEN");
    public static final EventType<FileEvent> SAVE = new EventType<>(ANY, "SAVE");

    public FileEvent(EventType<FileEvent> eventType, File file) {
        super(eventType);
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    private final File file;
}
