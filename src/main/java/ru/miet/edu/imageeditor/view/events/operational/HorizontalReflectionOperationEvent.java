package ru.miet.edu.imageeditor.view.events.operational;

import ru.miet.edu.imageeditor.model.operational.HorizontalReflectionOperation;

public class HorizontalReflectionOperationEvent extends ImageOperationEvent {
    public HorizontalReflectionOperationEvent() {
        super(ImageOperationEvent.CURRENT, new HorizontalReflectionOperation());
    }
}
