package ru.miet.edu.imageeditor.view.events.operational;

import javafx.event.Event;
import javafx.event.EventType;
import ru.miet.edu.imageeditor.model.operational.ImageOperation;

public class ImageOperationEvent extends Event {
    public static final EventType<ImageOperationEvent> DISPLAYED_ONLY = new EventType<>(ANY, "DISPLAYED_ONLY");
    public static final EventType<ImageOperationEvent> CURRENT = new EventType<>(ANY, "CURRENT");

    protected ImageOperationEvent(EventType<ImageOperationEvent> eventType, ImageOperation imageOperation) {
        super(eventType);
        this.imageOperation = imageOperation;
    }

    public ImageOperation getImageOperation() {
        return imageOperation;
    }

    private final ImageOperation imageOperation;
}
