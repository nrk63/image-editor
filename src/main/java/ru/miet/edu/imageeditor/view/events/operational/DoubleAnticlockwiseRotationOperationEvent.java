package ru.miet.edu.imageeditor.view.events.operational;

import ru.miet.edu.imageeditor.model.operational.DoubleAnticlockwiseRotationOperation;

public class DoubleAnticlockwiseRotationOperationEvent extends ImageOperationEvent {
    public DoubleAnticlockwiseRotationOperationEvent() {
        super(ImageOperationEvent.CURRENT, new DoubleAnticlockwiseRotationOperation());
    }
}
