package ru.miet.edu.imageeditor.view.events.operational;

import ru.miet.edu.imageeditor.model.operational.SepiaOperation;

public class SepiaOperationEvent extends ImageOperationEvent {
    public SepiaOperationEvent() {
        super(CURRENT, new SepiaOperation());
    }
}
