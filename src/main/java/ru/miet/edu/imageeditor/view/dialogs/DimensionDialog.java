package ru.miet.edu.imageeditor.view.dialogs;

import javafx.geometry.Dimension2D;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;

import java.util.ResourceBundle;

public class DimensionDialog extends Dialog<Dimension2D> {
    public DimensionDialog(ResourceBundle resourceBundle) {
        DialogPane dialogPane = getDialogPane();
        setTitle(resourceBundle.getString("dimensionDialog.title"));

        ButtonType applyButtonType = new ButtonType(
                resourceBundle.getString("dimensionDialog.applyButton.text"),
                ButtonBar.ButtonData.APPLY);
        ButtonType cancelButtonType = new ButtonType(
                resourceBundle.getString("dimensionDialog.cancelButton.text"),
                ButtonBar.ButtonData.CANCEL_CLOSE);
        dialogPane.getButtonTypes().addAll(applyButtonType, cancelButtonType);

        TextField widthTextField = new TextField();
        widthTextField.setPromptText(
                resourceBundle.getString("dimensionDialog.widthTextField.placeholder"));
        TextField heightTextField = new TextField();
        heightTextField.setPromptText(
                resourceBundle.getString("dimensionDialog.heightTextField.placeholder"));
        dialogPane.setContent(new VBox(widthTextField, heightTextField));

        setResultConverter(buttonType -> {
            if (buttonType == applyButtonType) {
                try {
                    double width = Double.parseDouble(widthTextField.getText());
                    double height = Double.parseDouble(heightTextField.getText());
                    if (width > 0.0 && height > 0.0) {
                        return new Dimension2D(width, height);
                    } else {
                        String warningText = resourceBundle.getString(
                                "dimensionDialog.warning.negativeArguments.text");
                        Alert alert = new WarningAlert(resourceBundle, warningText);
                        alert.showAndWait();
                    }
                } catch (NumberFormatException e) {
                    if (widthTextField.getText().isEmpty()) {
                        String warningText = resourceBundle.getString(
                                "dimensionDialog.warning.noWidth.text");
                        Alert alert = new WarningAlert(resourceBundle, warningText);
                        alert.showAndWait();
                    } else if (heightTextField.getText().isEmpty()) {
                        String warningText = resourceBundle.getString(
                                "dimensionDialog.warning.noHeight.text");
                        Alert alert = new WarningAlert(resourceBundle, warningText);
                        alert.showAndWait();
                    } else {
                        String warningText = resourceBundle.getString(
                                "dimensionDialog.warning.incorrectData.text");
                        Alert alert = new WarningAlert(resourceBundle, warningText);
                        alert.showAndWait();
                    }
                }
            }
            return null;
        });
    }
}
