package ru.miet.edu.imageeditor.view.events.operational;

import ru.miet.edu.imageeditor.model.operational.BlackAndWhiteOperation;

public class BlackAndWhiteOperationEvent extends ImageOperationEvent {
    public BlackAndWhiteOperationEvent() {
        super(CURRENT, new BlackAndWhiteOperation());
    }
}
