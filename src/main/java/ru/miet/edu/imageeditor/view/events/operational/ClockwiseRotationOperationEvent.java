package ru.miet.edu.imageeditor.view.events.operational;

import ru.miet.edu.imageeditor.model.operational.ClockwiseRotationOperation;

public class ClockwiseRotationOperationEvent extends ImageOperationEvent {
    public ClockwiseRotationOperationEvent() {
        super(CURRENT, new ClockwiseRotationOperation());
    }
}
