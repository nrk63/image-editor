package ru.miet.edu.imageeditor.view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Window extends Application {
    public void start(Stage stage) throws IOException, NullPointerException {
        stage.setMinWidth(MIN_SCREEN_WIDTH);
        stage.setMinHeight(MIN_SCREEN_HEIGHT);
        loadScene(stage, (new LoadSceneParams.Builder()).build());
        stage.show();
    }

    private void loadScene(Stage stage, LoadSceneParams params) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(WINDOW_RESOURCES_URL);
        ResourceBundle resourceBundle = ResourceBundle.getBundle("bundles.strings",
                                                                 params.getLocale());

        stage.setTitle(resourceBundle.getString("title"));
        fxmlLoader.setResources(resourceBundle);

        Parent root = fxmlLoader.load();
        WindowController controller = fxmlLoader.getController();
        controller.setLoadSceneMethod(params1 -> {
            try {
                loadScene(stage, params1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        controller.setProgramState(params.getProgramState());
        stage.setOnCloseRequest(event -> controller.shutdown());

        Scene scene = stage.getScene();
        if (scene == null)
            stage.setScene(new Scene(root));
        else
            scene.setRoot(root);
    }

    private static final double MIN_SCREEN_WIDTH = 600.0;
    private static final double MIN_SCREEN_HEIGHT = 600.0;
    //FIXME: wtf?
    private final URL WINDOW_RESOURCES_URL = getClass().getResource("../../../../../fxml/Window.fxml");
}
