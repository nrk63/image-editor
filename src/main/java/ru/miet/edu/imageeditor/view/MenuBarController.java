package ru.miet.edu.imageeditor.view;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuBar;
import ru.miet.edu.imageeditor.view.dialogs.DimensionDialog;
import ru.miet.edu.imageeditor.view.events.FileEvent;
import ru.miet.edu.imageeditor.view.events.operational.*;
import ru.miet.edu.imageeditor.view.events.LanguageChangeEvent;

import java.io.File;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

public class MenuBarController implements Initializable {
    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;
    }

    @FXML
    private void openImage() {
        File file = FileChoosers.showOpenImageChooser(resourceBundle);
        if (file == null)
            return;
        menuBar.fireEvent(new FileEvent(FileEvent.OPEN, file));
    }

    @FXML
    private void saveImage() {
        File file = FileChoosers.showSaveImageChooser(resourceBundle);
        if (file == null)
            return;
        menuBar.fireEvent(new FileEvent(FileEvent.SAVE, file));
    }

    <T extends Event> void addMenuBarEventHandler(EventType<T> eventType,
                                                  EventHandler<? super T> eventHandler) {
        menuBar.addEventHandler(eventType, eventHandler);
    }

    @FXML
    private void changeLanguageToEnglish() {
        menuBar.fireEvent(new LanguageChangeEvent(LanguageChangeEvent.CHANGED, englishLocale));
    }

    @FXML
    private void changeLanguageToRussian() {
        menuBar.fireEvent(new LanguageChangeEvent(LanguageChangeEvent.CHANGED, russianLocale));
    }

    @FXML
    private void onResizeOperationApplied() {
        DimensionDialog dialog = new DimensionDialog(resourceBundle);
        dialog.showAndWait().ifPresent(dim ->
                menuBar.fireEvent(new ResizeOperationEvent(dim.getWidth(), dim.getHeight())));
    }

    @FXML
    private void onHorizontalReflectionApplied() {
        menuBar.fireEvent(new HorizontalReflectionOperationEvent());
    }

    @FXML
    private void onVerticalReflectionApplied() {
        menuBar.fireEvent(new VerticalReflectionOperationEvent());
    }

    @FXML
    private void onClockwiseRotationApplied() {
        menuBar.fireEvent(new ClockwiseRotationOperationEvent());
    }

    @FXML
    private void onDoubleClockwiseRotationApplied() {
        menuBar.fireEvent(new DoubleClockwiseRotationOperationEvent());
    }

    @FXML
    private void onAnticlockwiseRotationApplied() {
        menuBar.fireEvent(new AnticlockwiseRotationOperationEvent());
    }

    @FXML
    private void onDoubleAnticlockwiseRotationApplied() {
        menuBar.fireEvent(new DoubleAnticlockwiseRotationOperationEvent());
    }

    @FXML
    private void onBlackAndWhiteFilterApplied() {
        menuBar.fireEvent(new BlackAndWhiteOperationEvent());
    }

    @FXML
    private void onSepiaFilterApplied() {
        menuBar.fireEvent(new SepiaOperationEvent());
    }

    @FXML
    private void onNegativeFilterApplied() {
        menuBar.fireEvent(new NegativeOperationEvent());
    }

    @FXML
    private MenuBar menuBar;
    private ResourceBundle resourceBundle;

    private static final Locale englishLocale = new Locale("en", "US");
    private static final Locale russianLocale = new Locale("ru", "RU");
}
