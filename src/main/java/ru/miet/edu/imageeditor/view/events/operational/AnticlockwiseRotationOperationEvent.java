package ru.miet.edu.imageeditor.view.events.operational;

import ru.miet.edu.imageeditor.model.operational.AnticlockwiseRotationOperation;

public class AnticlockwiseRotationOperationEvent extends ImageOperationEvent {
    public AnticlockwiseRotationOperationEvent() {
        super(ImageOperationEvent.CURRENT, new AnticlockwiseRotationOperation());
    }
}
