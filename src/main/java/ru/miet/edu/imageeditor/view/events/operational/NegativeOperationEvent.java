package ru.miet.edu.imageeditor.view.events.operational;

import ru.miet.edu.imageeditor.model.operational.NegativeOperation;

public class NegativeOperationEvent extends ImageOperationEvent {
    public NegativeOperationEvent() {
        super(CURRENT, new NegativeOperation());
    }
}
