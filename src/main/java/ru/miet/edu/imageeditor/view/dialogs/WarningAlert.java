package ru.miet.edu.imageeditor.view.dialogs;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;

import java.util.ResourceBundle;

public class WarningAlert extends Alert {
    public WarningAlert(ResourceBundle resourceBundle, String contentText) {
        super(AlertType.WARNING);
        ButtonType alertConfirmButtonType = new ButtonType(
                resourceBundle.getString("warningAlert.confirmButton.text"),
                ButtonBar.ButtonData.OK_DONE);
        getButtonTypes().setAll(alertConfirmButtonType);
        setTitle(resourceBundle.getString("warningAlert.title"));
        setHeaderText(resourceBundle.getString("warningAlert.headerText"));
        setContentText(contentText);
    }
}
