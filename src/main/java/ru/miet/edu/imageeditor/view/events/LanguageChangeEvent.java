package ru.miet.edu.imageeditor.view.events;

import javafx.event.Event;
import javafx.event.EventType;

import java.util.Locale;

public class LanguageChangeEvent extends Event {
    public static final EventType<LanguageChangeEvent> CHANGED = new EventType<>(ANY, "CHANGED");

    public LanguageChangeEvent(EventType<LanguageChangeEvent> eventType, Locale locale) {
        super(eventType);
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

    private final Locale locale;
}
